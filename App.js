/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import { StyleSheet } from "react-native";
import { Provider } from "react-redux";
import store from "./src/redux/store";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import TabRoutes from "./src/navigation/TabRoutes";

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <TabRoutes />
    </Provider>
  );
};

const styles = StyleSheet.create({});

export default App;
