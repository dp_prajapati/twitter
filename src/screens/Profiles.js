import {
  Text,
  FlatList,
  SafeAreaView,
  Image,
  View,
  Pressable,
  ActivityIndicator,
  Button,
} from "react-native";

import React from "react";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { globalStyles } from "../styles/global.js";
import { fetchProfiles } from "../redux/actions";
import Profile from "../components/Profile";
import MainProfile from "../components/MainProfile.js";

function Profiles({ dispatch, profiles, navigation }) {
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [profileIdx, setProfileIdx] = useState(0);
  useEffect(() => {
    dispatch(fetchProfiles(page, setIsLoading));
  }, [page]);

  const handleLoadMore = () => {
    setPage(page + 1);
  };

  const renderLoader = () => {
    return isLoading ? (
      <View style={globalStyles.loader}>
        <ActivityIndicator size="large" color="#aaa" />
      </View>
    ) : null;
  };
  return (
    <SafeAreaView style={{ backgroundColor: "#214f77" }}>
      {profiles.length > 0 ? (
        <MainProfile
          profile={{ ...profiles[profileIdx], index: profileIdx, navigation }}
        />
      ) : (
        <></>
      )}
      <FlatList
        data={profiles}
        renderItem={({ item, index }) => {
          return (
            <Pressable
              onPress={() => {
                setProfileIdx(index);
              }}
            >
              <Profile item={item} />
            </Pressable>
          );
        }}
        ListFooterComponent={renderLoader}
        keyExtractor={(item) => item.id}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0}
      />
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => {
  return {
    profiles: state.profiles,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profiles);
