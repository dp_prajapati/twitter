import React from "react";
import {
  Button,
  Image,
  TextInput,
  View,
  StyleSheet,
  Pressable,
  Text,
  TouchableOpacity,
  Platform,
} from "react-native";
import { useState } from "react";
import { globalStyles } from "../styles/global.js";
import { Formik } from "formik";
import { editProfile } from "../redux/actions";
import { useDispatch } from "react-redux";
import navigationStrings from "../navigation/navigationStrings.js";
import BottomSheet from "reanimated-bottom-sheet";
import Animated from "react-native-reanimated";
import ImagePicker from "react-native-image-crop-picker";

function EditProfile({ navigation, route }) {
  const dispatch = useDispatch();
  const { index, name, bio, email, profilePic } = route.params;
  const inputStyle = globalStyles.input;
  const [image, setImage] = useState(profilePic.replace("http", "https"));

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      setImage(image.path);
      this.bs.current.snapTo(1);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      setImage(image.path);
      this.bs.current.snapTo(1);
    });
  };

  const renderInner = () => (
    <View style={styles.panel}>
      <View style={{ alignItems: "center" }}>
        <Text style={styles.panelTitle}>Upload Photo</Text>
        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
      </View>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={takePhotoFromCamera}
      >
        <Text style={styles.panelButtonTitle}>Take Photo</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={choosePhotoFromLibrary}
      >
        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => bs.current.snapTo(1)}
      >
        <Text style={styles.panelButtonTitle}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  const bs = React.createRef();
  const fall = new Animated.Value(1);

  return (
    <View style={globalStyles.container}>
      <BottomSheet
        ref={bs}
        snapPoints={[330, 0]}
        renderContent={renderInner}
        renderHeader={renderHeader}
        initialSnap={1}
        callbackNode={fall}
        enabledGestureInteraction={false}
      />
      <Animated.View
        style={{
          margin: 20,
          opacity: Animated.add(0.1, Animated.multiply(fall, 1.0)),
        }}
      >
        <Formik
          initialValues={{
            name,
            bio,
            email,
          }}
          onSubmit={(values, actions) => {
            values.profilePic = image;
            actions.resetForm();
            dispatch(editProfile(index, values));
            navigation.navigate(navigationStrings.PROFILES);
          }}
        >
          {(props) => (
            <View>
              <Pressable onPress={() => bs.current.snapTo(0)}>
                <Image style={styles.imageStyle} source={{ uri: image }} />
              </Pressable>
              <TextInput
                style={inputStyle}
                placeholder="name"
                onChangeText={props.handleChange("name")}
                value={props.values.name}
              />

              <TextInput
                style={inputStyle}
                placeholder="email"
                onChangeText={props.handleChange("email")}
                value={props.values.email}
              />

              <TextInput
                style={inputStyle}
                placeholder="bio"
                onChangeText={props.handleChange("bio")}
                value={props.values.bio}
              />

              <Button
                color="maroon"
                title="Submit"
                onPress={props.handleSubmit}
              />
            </View>
          )}
        </Formik>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
  },
  header: {
    backgroundColor: "#FFFFFF",
    shadowColor: "#333333",
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: "gray",
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "#374875",
    alignItems: "center",
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  imageStyle: {
    width: 70,
    height: 70,
    alignSelf: "center",
    borderRadius: 10,
  },
});

export default EditProfile;
