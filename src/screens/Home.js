import {
  Text,
  Pressable,
  View,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Modal,
  ActivityIndicator,
} from "react-native";
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import MIcon from "react-native-vector-icons/MaterialIcons";
import TweetForm from "./TweetForm";
import { fetchTweets } from "../redux/actions";
import Tweet from "../components/Tweet";
import navigationStrings from "../navigation/navigationStrings.js";
import { globalStyles } from "../styles/global";
function Home({ dispatch, tweets, navigation }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);
  useEffect(() => {
    dispatch(fetchTweets(page, setIsLoading));
  }, [page]);

  const handleLoadMore = () => {
    setPage(page + 1);
  };

  const renderLoader = () => {
    return isLoading ? (
      <View style={globalStyles.loader}>
        <ActivityIndicator size="large" color="#aaa" />
      </View>
    ) : null;
  };
  return (
    <SafeAreaView style={{ backgroundColor: "#214f77" }}>
      <Modal visible={modalOpen} animationType="slide">
        <View style={styles.modalContent}>
          <MIcon
            name="close"
            size={24}
            style={{ ...styles.modalToggle, ...styles.modalClose }}
            onPress={() => setModalOpen(false)}
          />
          <TweetForm />
        </View>
      </Modal>
      <MIcon
        name="add"
        size={24}
        style={styles.modalToggle}
        onPress={() => setModalOpen(true)}
      />
      <FlatList
        data={tweets}
        renderItem={({ item, index }) => {
          return (
            <Pressable
              onPress={() =>
                navigation.navigate(navigationStrings.TWEET_DETAILS, {
                  tweet: item,
                })
              }
            >
              <Tweet item={item} index={index} dispatch={dispatch} />
            </Pressable>
          );
        }}
        ListFooterComponent={renderLoader}
        keyExtractor={(item) => item.id}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  modalToggle: {
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 10,
    borderWidth: 1,
    borderColor: "#f2f2f2",
    padding: 10,
    borderRadius: 10,
    alignSelf: "center",
  },
  modalClose: {
    marginTop: 40,
    marginBottom: 0,
  },
  modalContent: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    tweets: state.tweets,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
