import React from "react";
import { Button, TextInput, View, Text } from "react-native";
import { globalStyles } from "../styles/global.js";
import { Formik } from "formik";
import { connect } from "react-redux";
import { addTweet } from "../redux/actions";

function TweetForm(props) {
  return (
    <View style={globalStyles.container}>
      <Formik
        initialValues={{
          username: "",
          likes: "",
          comments: "",
          createdBy: "",
          userProfilePic: "",
        }}
        onSubmit={(values, actions) => {
          values.userProfilePic = "http://placeimg.com/640/480/people";
          actions.resetForm();
          props.dispatch(addTweet(values));
        }}
      >
        {(props) => (
          <View>
            <TextInput
              style={globalStyles.input}
              placeholder="username"
              onChangeText={props.handleChange("username")}
              value={props.values.username}
            />

            <TextInput
              style={globalStyles.input}
              placeholder="createdBy"
              onChangeText={props.handleChange("createdBy")}
              value={props.values.createdBy}
            />

            <TextInput
              style={globalStyles.input}
              placeholder="likes"
              onChangeText={props.handleChange("likes")}
              value={props.values.likes}
            />

            <TextInput
              style={globalStyles.input}
              placeholder="comments"
              onChangeText={props.handleChange("comments")}
              value={props.values.comments}
            />

            <Button
              color="maroon"
              title="Submit"
              onPress={props.handleSubmit}
            />
          </View>
        )}
      </Formik>
    </View>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: dispatch,
  };
};

export default connect(null, mapDispatchToProps)(TweetForm);
