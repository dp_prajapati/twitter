import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { globalStyles } from "../styles/global";
import Card from "../components/Card";
import Icon from "react-native-vector-icons/FontAwesome5";

function TweetDetails({ route }) {
  const { tweet } = route.params;
  const { text } = globalStyles;
  return (
    <>
      <View style={styles.container}>
        <Image
          source={{ uri: tweet.userProfilePic.replace("http", "https") }}
          style={styles.imageContainer}
        />

        <Card style={{ height: 350 }}>
          <View style={{ flex: 1, justifyContent: "space-around" }}>
            <View style={{ alignSelf: "center" }}>
              <Text style={styles.textContainer}>
                username : {tweet.username}
              </Text>
              <Text style={styles.textContainer}>
                createdBy : {tweet.createdBy}
              </Text>
              <Text style={styles.textContainer}>
                content : {tweet.content}
              </Text>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-around" }}
            >
              <View>
                <Icon
                  style={styles.icon}
                  name="heart"
                  color="white"
                  size={25}
                />
                <Text style={styles.textContainer}>{tweet.likes}</Text>
              </View>
              <View>
                <Icon
                  name="comment"
                  style={styles.icon}
                  color="white"
                  size={25}
                />
                <Text style={styles.textContainer}>{tweet.comments}</Text>
              </View>
            </View>
          </View>
        </Card>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#214f77",
  },
  imageContainer: {
    height: "40%",
    width: "80%",
    alignSelf: "center",
    margin: 15,
    borderRadius: 20,
  },
  textContainer: {
    ...globalStyles.text,
    fontSize: 20,
  },
  icon: { alignSelf: "center", marginBottom: 5 },
});

export default TweetDetails;
