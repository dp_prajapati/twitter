import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    borderWidth: 1,
    borderColor: "#ddd",
    padding: 10,
    margin: 5,
    fontSize: 18,
    borderRadius: 6,
  },
  contentContainer: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-around",
  },

  imageContainer: {
    height: 70,
    width: 70,
    backgroundColor: "red",
    borderRadius: 35,
  },
  text: {
    color: "white",
  },
  header: {
    color: "white",
    fontWeight: "800",
    fontSize: 25,
    alignSelf: "center",
  },
  loader: {
    marginVertical: 16,
    alignItems: "center",
  },
});
