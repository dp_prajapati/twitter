import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Home, TweetDetails } from "../screens";
import navigationStrings from "./navigationStrings";

const Stack = createStackNavigator();

export default function TweetStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name={navigationStrings.TWEETS} component={Home} />
      <Stack.Screen
        name={navigationStrings.TWEET_DETAILS}
        component={TweetDetails}
      />
    </Stack.Navigator>
  );
}
