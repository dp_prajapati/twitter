export default {
  PROFILES: "Profiles",
  TABS: "Tabs",
  TWEETS: "Tweets",
  TWEET_DETAILS: "TweetDetails",
  EDIT_PROFILE: "EditProfile",
};
