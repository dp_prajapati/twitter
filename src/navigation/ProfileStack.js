import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Profiles } from "../screens";
import navigationStrings from "./navigationStrings";
import EditProfile from "../screens/EditProfile";

const Stack = createStackNavigator();

export default function ProfileStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name={navigationStrings.PROFILES} component={Profiles} />
      <Stack.Screen
        name={navigationStrings.EDIT_PROFILE}
        component={EditProfile}
      />
    </Stack.Navigator>
  );
}
