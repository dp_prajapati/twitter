import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import navigationStrings from "./navigationStrings";
import TweetStack from "./TweetStack";
import ProfileStack from "./ProfileStack";
import Icon from "react-native-vector-icons/FontAwesome5";
const Tab = createBottomTabNavigator();
import { NavigationContainer } from "@react-navigation/native";
function TabRoutes() {
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={{ headerShown: false }}>
        <Tab.Screen
          options={{
            tabBarIcon: ({ focused, size }) => {
              size = focused ? 25 : 20;
              return <Icon name="twitter" size={size} color="#00acee" />;
            },
          }}
          name={navigationStrings.TWEETS}
          component={TweetStack}
        />
        <Tab.Screen
          options={{
            tabBarIcon: ({ focused, size }) => {
              size = focused ? 25 : 20;
              return <Icon name="user" size={size} color="#00acee" />;
            },
          }}
          name={navigationStrings.PROFILES}
          component={ProfileStack}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default TabRoutes;
