import React from "react";
import Card from "./Card";
import { globalStyles } from "../styles/global.js";
import { Text, View, Image, Pressable } from "react-native";
function Profile(props) {
  const { item } = props;
  return (
    <Card>
      <View style={globalStyles.contentContainer}>
        <View>
          <Image
            style={globalStyles.imageContainer}
            source={{
              uri: item.profilePic.replace("http", "https"),
            }}
          />
        </View>
        <View
          style={{
            width: "60%",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Text style={globalStyles.text}>{item.name}</Text>
          <View
            style={{ backgroundColor: "white", padding: 10, borderRadius: 20 }}
          >
            <Text>follow</Text>
          </View>
        </View>
      </View>

      <View style={globalStyles.contentContainer}>
        <Text style={globalStyles.text}>
          <Text style={{ fontWeight: "bold" }}>{item.followers}</Text> followers
        </Text>
        <Text style={globalStyles.text}>
          <Text style={{ fontWeight: "bold" }}>{item.following}</Text> following
        </Text>
      </View>
    </Card>
  );
}

export default Profile;
