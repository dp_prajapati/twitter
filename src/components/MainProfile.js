import React from "react";
import { Button, Image, StyleSheet, Text, View } from "react-native";
import navigationStrings from "../navigation/navigationStrings";
import { globalStyles } from "../styles/global";
import Card from "./Card";

function MainProfile({ profile }) {
  const {
    profilePic,
    name,
    bio,
    email,
    following,
    followers,
    index,
    navigation,
  } = profile;
  return (
    <Card style={{ height: 300, backgroundColor: "#021227" }}>
      <Image
        style={styles.imageContainer}
        source={{
          uri: profilePic.replace("http", "https"),
        }}
      />
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text
          style={{
            ...globalStyles.text,
            fontSize: 25,
            marginBottom: 10,
          }}
        >
          {name}
        </Text>
        <Text style={styles.textStyle}>{email}</Text>
        <Text style={styles.textStyle}>{bio}</Text>
        <Button
          title="Edit Profile"
          onPress={() =>
            navigation.navigate(navigationStrings.EDIT_PROFILE, {
              index,
              name,
              bio,
              email,
              profilePic,
            })
          }
        />
      </View>
      <View style={{ ...globalStyles.contentContainer, marginVertical: 5 }}>
        <Text style={globalStyles.text}>
          <Text style={{ fontWeight: "bold" }}>{followers}</Text> followers
        </Text>
        <Text style={globalStyles.text}>
          <Text style={{ fontWeight: "bold" }}>{following}</Text> following
        </Text>
      </View>
    </Card>
  );
}

const styles = StyleSheet.create({
  imageContainer: {
    ...globalStyles.imageContainer,
    alignSelf: "center",
    height: 100,
    width: 100,
    borderRadius: 50,
    marginTop: 10,
  },
  textStyle: {
    ...globalStyles.text,
    fontSize: 15,
    marginBottom: 5,
  },
});

export default MainProfile;
