import React from "react";
import { View, StyleSheet } from "react-native";
function Card(props) {
  if (props.style === undefined) props.style = {};
  return (
    <View style={{ ...styles.card, ...props.style }}>{props.children}</View>
  );
}

const styles = StyleSheet.create({
  card: {
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.3,
    backgroundColor: "white",
    height: 120,
    marginVertical: 10,
    borderRadius: 5,
    marginHorizontal: 10,
    padding: 5,
    backgroundColor: "#09233a",
  },
});

export default Card;
