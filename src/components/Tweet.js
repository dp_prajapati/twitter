import React from "react";
import Card from "../components/Card";
import { globalStyles } from "../styles/global.js";
import { Text, View, Image, Pressable } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { increaseLikes } from "../redux/actions";
import MIcon from "react-native-vector-icons/MaterialIcons";
function Tweet(props) {
  const { item, index, dispatch } = props;
  return (
    <Card>
      <View style={globalStyles.contentContainer}>
        <View>
          <Image
            style={globalStyles.imageContainer}
            source={{
              uri: item.userProfilePic.replace("http", "https"),
            }}
          />
        </View>
        <View style={{ width: "60%", justifyContent: "space-around" }}>
          <Text style={globalStyles.text}>createdBy: {item.createdBy}</Text>
        </View>
      </View>

      <View style={globalStyles.contentContainer}>
        <Text style={{ ...globalStyles.text }}>
          {!item.isLiked ? (
            <Icon
              onPress={() => {
                !item.isLiked ? dispatch(increaseLikes(index)) : null;
              }}
              name="heart"
              size={15}
            />
          ) : (
            <MIcon style={{ color: "red" }} name="favorite" size={15} />
          )}{" "}
          {item.likes}
        </Text>

        <Text style={globalStyles.text}>
          <Icon name="comment" size={15} /> {item.comments}
        </Text>
      </View>
    </Card>
  );
}

export default Tweet;
