export const SET_TWEETS = "SET_TWEETS";
export const ADD_TWEET = "ADD_TWEET";
export const SET_PROFILES = "SET_PROFILES";
export const INCREASE_LIKES = "INCREASE_LIKES";
export const EDIT_PROFILE = "EDIT_PROFILE";
