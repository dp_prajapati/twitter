import {createStore, applyMiddleware} from 'redux';
import rootReducer from './rootReducer';
import thunk from 'redux-thunk';
import {setTweets} from './actions';
import {setProfiles} from './actions';

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
