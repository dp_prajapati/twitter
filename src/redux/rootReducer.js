import { Alert } from "react-native";
import * as actions from "./actionTypes";

const initialTasks = {
  tweets: [],
  profiles: [],
};

const rootReducer = (state = initialTasks, action) => {
  switch (action.type) {
    case actions.SET_TWEETS: {
      return {
        ...state,
        tweets: [...state.tweets, ...action.payload.tweets],
      };
    }

    case actions.SET_PROFILES: {
      return {
        ...state,
        profiles: [...state.profiles, ...action.payload.profiles],
      };
    }

    case actions.EDIT_PROFILE: {
      const { index, profile } = action.payload;
      const Profiles = state.profiles.slice();
      Profiles[index] = { ...Profiles[index], ...profile };
      return {
        ...state,
        profiles: Profiles,
      };
    }

    case actions.ADD_TWEET: {
      return {
        ...state,
        tweets: [
          { ...action.payload.newTweet, id: state.tweets.length + 1 },
          ...state.tweets,
        ],
      };
    }

    case actions.INCREASE_LIKES: {
      const { index } = action.payload;
      const Tweets = state.tweets.slice();
      Tweets[index].isLiked = true;
      Tweets[index].likes = parseInt(Tweets[index].likes) + 1;
      return {
        ...state,
        tweets: Tweets,
      };
    }

    default:
      return state;
  }
};

export default rootReducer;
