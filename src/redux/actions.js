import * as action from "./actionTypes";
export function setTweets(tweets) {
  return {
    type: action.SET_TWEETS,
    payload: {
      tweets,
    },
  };
}
export function setProfiles(profiles) {
  return {
    type: action.SET_PROFILES,
    payload: {
      profiles,
    },
  };
}
export function addTweet(newTweet) {
  return {
    type: action.ADD_TWEET,
    payload: {
      newTweet,
    },
  };
}

export function increaseLikes(index) {
  return {
    type: action.INCREASE_LIKES,
    payload: {
      index,
    },
  };
}

export function editProfile(index, profile) {
  return {
    type: action.EDIT_PROFILE,
    payload: {
      index,
      profile,
    },
  };
}

export const fetchTweets = (page, setIsLoading) => {
  setIsLoading(true);
  return (dispatch) => {
    fetch(
      `https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/tweets?page=${page}&limit=10`
    )
      .then((response) => response.json())
      .then((tweets) => {
        tweets = tweets.map((tweet) => ({
          ...tweet,
          isLiked: false,
        }));
        dispatch(setTweets(tweets));
        setIsLoading(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };
};

export const fetchProfiles = (page, setIsLoading) => {
  setIsLoading(true);
  return (dispatch) => {
    fetch(
      `https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/users?page=${page}&limit=5`
    )
      .then((response) => response.json())
      .then((profiles) => {
        dispatch(setProfiles(profiles));
        setIsLoading(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };
};
